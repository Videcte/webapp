import './styles/index.scss'
import './js/index';
import { HTML5_FMT } from 'moment';
import { async } from 'q';
var moment = require('moment');

const apiKey = 'iACnZagL8HcpAXjcdhyHjjJHydLhbZJT';
let cityKey = undefined;

const images = importAll(require.context('./images', false, /\.(png|jpe?g|svg)$/));
const search_box = document.getElementsByClassName('search-box');
const navi_items = document.getElementsByClassName('navi-item');
const nav_btn = document.getElementById('nav-button');
const nav_wrap = document.getElementById('navi-wrapper');
const back_btn = document.getElementsByClassName('search-back-btn');
const sub_btn = document.getElementsByClassName('sub');
let nav_open = false;

document.getElementById('start-weather').addEventListener('click' , () => {
    getCity(todayWeather.location.latitude,todayWeather.location.longitude,undefined,undefined);
})

for(let i=0; i<back_btn.length; i++) {
    back_btn[i].addEventListener('click' , () => {
        search_box[i].style.transform = "rotateX(180deg)";
    })        
}

nav_btn.addEventListener('click', () => {
    let width = window.getComputedStyle(nav_wrap).width;
    width = parseInt(width,10);
    var rect = nav_wrap.getBoundingClientRect();
    if(nav_open === false) {
        if(rect.right > 250) {
            nav_wrap.style.transform = "translate(-50%, -30%)";
            nav_btn.innerText = "close nav";
            nav_btn.style.top = "5%";
            nav_open = true;
        }else {
            nav_wrap.style.transform = "translateX(-71px)";
            nav_btn.innerText = "close nav";
            nav_open = true;
         
        }
    }else 
    {
        if(rect.right > 250) {
            if(window.innerHeight > 900 && window.innerWidth < 900) {
                nav_wrap.style.transform = "translate(-50%, -226%)";    
            }
            nav_wrap.style.transform = "translate(-50%, -206%)";
            nav_btn.style.top = "17%";
            nav_btn.innerText = "open nav";
            nav_open = false;
        }
        else {
            nav_wrap.style.transform = "translateX(-250px)";
            nav_btn.innerText = "open nav";
            nav_open = false;    
        }
    }
})
for(let i=0; i<navi_items.length; i++) {
    navi_items[i].addEventListener('click' , () => {
        search_box[i].style.transform = "rotateX(0)";
    })        
}

for(let i=0; i<sub_btn.length; i++) {
    sub_btn[i].addEventListener('click' , (e) => {
        e.preventDefault();
        if(i === 0) {
            let lat = e.target.parentNode.children[1].value;
            let long = e.target.parentNode.children[2].value
            getCity(lat,long);
        }else if(i === 1) {
            let city = e.target.parentNode.children[1].value;
            getCity(undefined,undefined,city,undefined);
        }else if(i === 2) {
            let ip = e.target.parentNode.children[1].value;
            getCity(undefined,undefined,undefined,ip);
        }
        
    })        
}


function importAll(r) {
    let images = {};
    r.keys().map((item, index) => { images[item.replace('./', '')] = r(item); });
    return images;
}
let todayWeather = {
    location: {
      latitude: undefined,
      longitude: undefined
    },
    day: {},
    night: {},
    date: 'str',

    setLocation: function(lat , long) {
        this.location.latitude = lat;
        this.location.longitude = long;
    },
 
};

setTimeout( () => {
    if(navigator.geolocation) {
        navigator.geolocation.getCurrentPosition( (position) => {
            todayWeather.setLocation(position.coords.latitude , position.coords.longitude);
        })
    }
},1000)

async function getTodayWeather(key) {
       
    const response = await fetch(`http://dataservice.accuweather.com/forecasts/v1/daily/1day/${key}?apikey=${apiKey}&language=pl`);
    const json = response.json();
    return json;
}

async function getWeekWeather(key) {
    const response = await fetch(`http://dataservice.accuweather.com/forecasts/v1/daily/5day/${key}?apikey=${apiKey}&language=pl`)
    const json = await response.json();
    return json;
    
}
// F = (°C × 1.8) + 32
async function getData(key) {
    console.log(key);
    const data = await getTodayWeather(key);
    const weekData = await getWeekWeather(key);
    todayWeather['day'] = data['DailyForecasts'][0]['Day'];
    todayWeather['night'] = data['DailyForecasts'][0]['Night'];
         addHeaderToSite(data,weekData);
         addIconsToSite(data);
         addDataToWeekBox(weekData);
}

// p p p p p

async function getCity(lat,long,city,ip) {
    if(lat !== undefined && long !==undefined) {
        const response = await fetch(`http://dataservice.accuweather.com/locations/v1/cities/geoposition/search?apikey=${apiKey}&q=${lat}%2C%20${long}`)
        const localization = await response.json();
        if(localization !== undefined) {
            const name = localization['ParentCity']['LocalizedName'];
            cityKey = localization['ParentCity']['Key'];
                setCityName(name)
                getData(cityKey);
        }else console.log('Bad Request');
        
        //document.getElementById('geo-result').appendChild(text);    
    }else if(city !==undefined) {
        console.log(city);
        const response = await fetch(`http://dataservice.accuweather.com/locations/v1/cities/PL/search?apikey=${apiKey}&q=${city}`);
        console.log(response);
        if(response == undefined) {
            response = await fetch(`http://dataservice.accuweather.com/locations/v1/cities/search?apikey=${apiKey}&q=${city}`);
        }
        const localization = await response.json();
            const name = localization[0]['LocalizedName'];
            cityKey = localization[0]['Key'];
                setCityName(name)
                getData(cityKey);       
    }
    else if(ip !== undefined) {
        const response = await fetch(`http://dataservice.accuweather.com/locations/v1/cities/ipaddress?apikey=${apiKey}&q=${ip}`);
        const localization = await response.json();
        if(localization !== undefined) {
            const name = localization['LocalizedName'];
            cityKey = localization['Key'];
                setCityName(name)
                getData(cityKey);        
        } else console.log('Bad request');
    }
}
function setCityName(name) {
    const two = document.getElementsByClassName('weather-box');
    two[0].children[0].innerText = name;
    two[1].children[0].innerText = name;
}

function addHeaderToSite(data,weekData) {
  const p = document.getElementById('header');
  moment().format('LL');

   let headers = new Array(2);
        headers[0] = document.getElementById('day').querySelector('.header-primary');
        headers[1] = document.getElementById('night').querySelector('.header-primary');
             headers[0].innerText = `DAY TIME`
             headers[1].innerText = `NIGHT TIME`  
   
 todayWeather['date'] = moment(data['DailyForecasts'][0]['Date']).format("LL");
  p.innerHTML = `<h1>${todayWeather.date}</h1>`;
}

function addIconsToSite(data) {
    todayWeather.max = Math.ceil((data['DailyForecasts'][0]['Temperature']['Maximum']['Value'] - 32)/1.8);
    todayWeather.min = Math.ceil((data['DailyForecasts'][0]['Temperature']['Minimum']['Value'] - 32)/1.8);
        let tab = new Array(2);
            tab[0] = document.getElementById('day_text');
            tab[1] = document.getElementById('night_text');
                tab[0].innerHTML = `<h3>${data['DailyForecasts'][0]['Day']['IconPhrase']}`;
                tab[1].innerHTML = `<h3>${data['DailyForecasts'][0]['Night']['IconPhrase']}`;
                    
        let temps = document.getElementsByClassName('temp_box');
            temps[0].innerHTML = `${todayWeather.max} &#8451`;
            temps[1].innerHTML = `${todayWeather.min} &#8451`;
       
                  setIcon('night' , data);
                  setIcon('day' , data); 
}

function addDataToWeekBox(weekData) {
    let imgInList = [
        Array.from(document.getElementById('night').getElementsByClassName('week-icon-img')),
        Array.from(document.getElementById('day').getElementsByClassName('week-icon-img'))
    ]
    let dateInList = [
        Array.from(document.getElementById('night').querySelector('.week_day-list').querySelectorAll('li')),
        Array.from(document.getElementById('day').querySelector('.week_day-list').querySelectorAll('li'))
    ]
    let tempInList = [
        Array.from(document.getElementById('night').querySelector('.week_temp-list').querySelectorAll('li')),
        Array.from(document.getElementById('day').querySelector('.week_temp-list').querySelectorAll('li'))
    ]
    
    
    // Day info
    let bigArray = [new Array(0) , new Array(0)];
   
    for(let k of weekData['DailyForecasts']) {
        let bigObj = [
            {
                date: moment(k['Date']).format("L"),
                icon: k['Day']['Icon'],
                day: moment(k['Date']).format('dddd'),
                min: k['Temperature']['Minimum'],
                max: k['Temperature']['Maximum']
            },
            {
                min: k['Temperature']['Minimum'],
                max: k['Temperature']['Maximum'],
                icon: k['Night']['Icon']
            }
        ]
            bigArray[0].push(bigObj[0]);
            bigArray[1].push(bigObj[1]);
    }
        for(let k=0; k<imgInList[0].length; k++) {

            setWeekIcons(imgInList[0][k],bigArray[0][k]);
            setWeekIcons(imgInList[1][k],bigArray[1][k]);
            setDateInList(dateInList[0][k],bigArray[0][k]['date'] , bigArray[0][k]['day']);
            setDateInList(dateInList[1][k],bigArray[0][k]['date'] , bigArray[0][k]['day']);
            setTempInList(tempInList[0][k],bigArray[0][k]['min'] , bigArray[0][k]['max']);
            setTempInList(tempInList[1][k],bigArray[1][k]['min'] , bigArray[1][k]['max']);
        }
}

function setTempInList(tempListItem , min , max) {
   let headers = tempListItem.getElementsByTagName('h2');
   headers[0].innerHTML = `${Math.ceil((min['Value']- 32)/1.8)} &deg`;
   headers[1].innerHTML = `${Math.ceil((max['Value']- 32)/1.8)} &deg`;
    
}

function setDateInList(dateInList , date , day) {

    dateInList.querySelector('p').innerText = day;
    dateInList.querySelector('h5').innerText = date;
    dateInList.querySelector('h5').classList.add('zeroMargin');
}

function setWeekIcons(imgInList , info) {
    // imgInList is a reference to img in icon-list
    // info is element of array containing date and icon number
    let iconString = null;
    let nmb = parseInt(info['icon'],10);
    if( nmb < 10) {
        iconString =`0${info['icon']}-s.png`;        
    }else{
        iconString =`${info['icon']}-s.png`;
    }
    for(let k in images) {
        if(k == iconString){
            imgInList.src = images[k];
        }
    }

}

function setIcon(str , data) {
    let container = null;
    let icon = null;
    let iconString=null;

        if(str == 'night') {
           container = document.getElementById('night_img') 
           icon = todayWeather.night['Icon']; 
        }else {
            container = document.getElementById('day_img')
            icon = todayWeather.day['Icon'];
        }
         
    let nmb = parseInt(icon,10);
    if( nmb < 10) {
        iconString =`0${icon}-s.png`;        
    }else{
        iconString =`${icon}-s.png`;
    }
    for(let k in images) {
        if(k == iconString){
            container.src = images[k];
        }
    }

}